function getClientes(){
  //var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function () {
    if(this.readyState == 4 && this.status == 200){
    //  console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);
//  alert(JSONProductos.value[0].ProductName);
var divTabla= document.getElementById("divTablaC");
var tabla= document.createElement("table");
var tbody= document.createElement("tbody");

tabla.classList.add("table");
tabla.classList.add("table-striped");

//NOMBRE CIUDAD Y BANDERA
  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;
    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;
    var columnaBandera = document.createElement("td");
    var imagen =   document.createElement("img");
    imagen.classList.add("flag");

    if (JSONClientes.value[i].Country=="UK"){
      imagen.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png";
    }else{
      imagen.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONClientes.value[i].Country+".png";
    }

    columnaBandera.appendChild(imagen);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);
    tbody.appendChild(nuevaFila);
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
  }
